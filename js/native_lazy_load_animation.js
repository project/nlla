'use strict';

(function ($, Drupal) {

  Drupal.behaviors.nativeLazyLoadAnimation = {
    attach: function (context) {
      $('img.lazyload').once('lazy-load-animation').on('load', function () {
        $(this).addClass('loaded');
      }).each(function () {
        if (this.complete) {
          $(this).trigger('load');
        }
      });
    }
  };

})(jQuery, Drupal);
